package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	"ebay-go/config"
	"ebay-go/ebay"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Tokens struct {
	AccessToken           string `json:"access_token"`
	ExpiresIn             int    `json:"expires_in"`
	RefreshToken          string `json:"refresh_token"`
	RefreshTokenExpiresIn int64  `json:"refresh_token_expires_in"`
	TokenType             string `json:"token_type"`
}

type StateHandler struct {
	config config.Config
	tokens Tokens
	db     *mongo.Database
}

type SessionData struct {
	ID     string            `bson:"_id,omitempty"`
	Tokens Tokens            `json:"tokens"`
	User   ebay.EbayIdentity `json:"user"`
}

func main() {
	r := chi.NewRouter()
	handler := StateHandler{
		config: config.Production(),
		db:     openDb("ebaydb"),
	}

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))

	r.Get("/ebay", handler.challenge)
	r.Post("/ebay", handler.deleteUser)
	r.Get("/ebay/login", handler.login)
	r.Get("/ebay/auth", handler.auth)
	r.Get("/ebay/protected", handler.protected)
	r.NotFound(hello)
	http.ListenAndServe(":3000", r)
}

func openDb(dbName string) *mongo.Database {
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbUri := fmt.Sprintf("mongodb+srv://%s:%s@ebaydb.9b992ek.mongodb.net/?retryWrites=true&w=majority", dbUser, dbPass)
	clientOptions := options.Client().ApplyURI(dbUri).
		SetServerAPIOptions(serverAPIOptions)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	dbClient, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	return dbClient.Database(dbName)
}

func logHeaders(label string, r *http.Request) {
	for key, val := range r.Header {
		log.Printf("  %s:    %s: %s", label, key, val)
	}

}

func hello(w http.ResponseWriter, r *http.Request) {
	logHeaders("hello", r)
	w.Write([]byte("Hello World!"))
}

type ChallengeResponse struct {
	Digest string `json:"challengeResponse,omitempty"`
}

func (state StateHandler) challenge(w http.ResponseWriter, r *http.Request) {
	compound := r.URL.Query().Get("challenge_code")
	compound += os.Getenv("VERIFICATION_TOKEN")
	compound += os.Getenv("ENDPOINT")
	log.Printf("  compound %s", compound)
	challResp := ebayDigest(compound)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(challResp)
}

func ebayDigest(compound string) ChallengeResponse {
	digest := sha256.Sum256([]byte(compound))
	challResp := ChallengeResponse{hex.EncodeToString(digest[:])}
	return challResp
}

func (state StateHandler) deleteUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Delete user"))
}

func (state StateHandler) login(w http.ResponseWriter, r *http.Request) {
	authURL := state.config.GenerateUserAuthURL()
	http.Redirect(w, r, authURL, http.StatusTemporaryRedirect)
}

func (state StateHandler) auth(w http.ResponseWriter, r *http.Request) {
	logHeaders("auth", r)

	code := r.URL.Query().Get("code")

	// Exchange the code for an access token from ebay
	tokenReq, err := state.config.GenerateExchangeRequest(code)
	if err != nil {
		log.Fatalf("Can't generate code exchange request")
	}
	tokenResp, err := http.DefaultClient.Do(tokenReq)
	if err != nil {
		log.Fatalf("Can't get code exchange response")
	}
	json.NewDecoder(tokenResp.Body).Decode(&state.tokens)

	// Get the user's identity from ebay
	idReq, err := state.config.GenerateIdentityRequest(state.tokens.AccessToken)
	if err != nil {
		log.Fatalf("Can't generate id request")
	}
	idResp, err := http.DefaultClient.Do(idReq)
	if err != nil {
		log.Fatalf("Can't get id response")
	}
	idBody, err := ioutil.ReadAll(idResp.Body)
	if err != nil {
		// handle error
	}
	idBodyString := string(idBody)
	log.Println(idBodyString)
	var ebayId ebay.EbayIdentity
	json.NewDecoder(bytes.NewReader(idBody)).Decode(&ebayId)
	log.Printf("%s id %s", ebayId.Username, ebayId.UserID)

	// Store the access token and put it in a cookie
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	collection := state.db.Collection("sessions")
	// if err := state.db.CreateCollection(ctx, "sessions"); err != nil {
	// 	if !mongo.Collectio.IsCollectionExists(err) {
	// 		log.Fatal(err)
	// 	}
	// }

	// Save session
	sessionID := uuid.New().String()
	if _, err := collection.InsertOne(ctx, bson.D{
		{Key: "_id", Value: sessionID},
		{Key: "lastModified", Value: time.Now()},
		{Key: "user", Value: ebayId},
		{Key: "tokens", Value: state.tokens}}); err != nil {
		log.Fatal(err)
	}
	// Get session later (why!)
	var sessionData bson.M
	if err := collection.FindOne(ctx, bson.M{"_id": sessionID}).Decode(&sessionData); err != nil {
		if err == mongo.ErrNoDocuments {
			// Session not found
		} else {
			log.Fatal(err)
		}
	}
	// Delete expired sessions
	if _, err := state.db.Collection("sessions").DeleteMany(ctx, bson.M{"lastModified": bson.M{"$lt": time.Now().Add(-state.config.SessionLen)}}); err != nil {
		log.Fatal(err)
	}

	// Put session id in cookie
	http.SetCookie(w, &http.Cookie{
		Name:   "SESSION",
		Value:  sessionID,
		MaxAge: int(state.config.SessionLen.Seconds()),
		Path:   "/",
	})
}

func (state StateHandler) protected(w http.ResponseWriter, r *http.Request) {
	logHeaders("prot", r)

	cookie, err := r.Cookie("SESSION")
	if err != nil {
		log.Print("Couldn't get SESSION cookie")
		notAuthed(state, w, r)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var sessionData SessionData
	if err := state.db.Collection("sessions").FindOne(ctx, bson.M{
		"_id":          cookie.Value,
		"lastModified": bson.M{"$gte": time.Now().Add(-state.config.SessionLen)},
	}).Decode(&sessionData); err != nil {
		if err == mongo.ErrNoDocuments {
			log.Printf("Couldn't find doc for SESSION id %s", cookie.Value)
			notAuthed(state, w, r)
		} else {
			log.Fatal(err)
		}
		return
	}

	w.Write([]byte(sessionData.User.BusinessAccount.Email))
}

func notAuthed(state StateHandler, w http.ResponseWriter, r *http.Request) {
	authURL := state.config.GenerateUserAuthURL()
	http.Redirect(w, r, authURL, http.StatusTemporaryRedirect)
}
