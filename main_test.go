package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestEbayDigest(t *testing.T) {
	code := "a8628072-3d33-45ee-9004-bee86830a22d"
	token := "71745723-d031-455c-bfa5-f90d11b4f20a"
	endp := "http://www.testendpoint.com/webhook"
	got := ebayDigest(code + token + endp)
	want := ChallengeResponse{"ca527df75caa230092d7e90484071e8f05d63068f1317973d6a3a42593734bbb"}
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

func TestDelEbayUser(t *testing.T) {
	req, err := http.NewRequest("DELETE", "/user/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Key", "Value")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(StateHandler{}.deleteUser)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := "Delete user"
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func TestChallenge(t *testing.T) {
	code := "a8628072-3d33-45ee-9004-bee86830a22d"
	token := "71745723-d031-455c-bfa5-f90d11b4f20a"
	endp := "http://www.testendpoint.com/webhook" // set up a test HTTP request with query parameters
	req, err := http.NewRequest("GET", "/challenge?challenge_code="+code, nil)
	if err != nil {
		t.Fatal(err)
	}

	// set environment variables needed for function
	os.Setenv("VERIFICATION_TOKEN", token)
	os.Setenv("ENDPOINT", endp)

	// set up a response recorder to capture the output of the function
	rr := httptest.NewRecorder()
	state := StateHandler{}
	handler := http.HandlerFunc(state.challenge)

	// call the function and capture the response
	handler.ServeHTTP(rr, req)

	// check the response status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	var challResp ChallengeResponse
	err = json.NewDecoder(rr.Body).Decode(&challResp)
	if err != nil {
		t.Errorf("error parsing response body: %v", err)
	}

	// check the response body
	if challResp.Digest != "ca527df75caa230092d7e90484071e8f05d63068f1317973d6a3a42593734bbb" {
		t.Errorf("handler returned unexpected body: got %v want %v", challResp.Digest, "ca527df75caa230092d7e90484071e8f05d63068f1317973d6a3a42593734bbb")
	}

	// unset the environment variables to avoid affecting other tests
	os.Unsetenv("VERIFICATION_TOKEN")
	os.Unsetenv("ENDPOINT")
}
