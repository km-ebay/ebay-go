package config

import (
	"net/http"
	"net/url"
	"os"
	"time"
)

type Config struct {
	// config_identifier string,
	web_endpoint      string
	api_endpoint      string
	identity_endpoint string
	scopes            string
	app_id            *string
	cert_id           *string
	ru_name           *string
	SessionLen        time.Duration
}

func init() {

}

func Production() Config {
	return Config{
		// config_identifier: "api.ebay.com",
		web_endpoint:      "https://auth.ebay.com/oauth2/authorize",
		api_endpoint:      "https://api.ebay.com/identity/v1/oauth2/token",
		identity_endpoint: "https://apiz.ebay.com/commerce/identity/v1/user",
		scopes: "https://api.ebay.com/oauth/api_scope " +
			"https://api.ebay.com/oauth/api_scope/sell.inventory " +
			"https://api.ebay.com/oauth/api_scope/sell.account.readonly " +
			"https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly " +
			"https://api.ebay.com/oauth/api_scope/sell.analytics.readonly " +
			"https://api.ebay.com/oauth/api_scope/commerce.identity.readonly " +
			"https://api.ebay.com/oauth/api_scope/sell.marketing.readonly",
		app_id:     func() *string { v := os.Getenv("EBAY_CLIENT_ID"); return &v }(),
		cert_id:    func() *string { v := os.Getenv("EBAY_CERT_ID"); return &v }(),
		ru_name:    func() *string { v := os.Getenv("EBAY_RU_NAME"); return &v }(),
		SessionLen: 1 * time.Hour,
	}
}

func (e Config) GenerateUserAuthURL() string {
	auth_url := e.web_endpoint + "?client_id=" + *e.app_id +
		"&response_type=code" +
		"&redirect_uri=" + *e.ru_name +
		"&scope=" + e.scopes
	return auth_url
}

func (e Config) GenerateExchangeRequest(code string) (*http.Request, error) {
	exch_body := url.Values{}
	exch_body.Set("grant_type", "authorization_code")
	exch_body.Set("redirect_uri", *e.ru_name)
	exch_body.Set("code", code)

	req, err := http.NewRequest("POST", e.api_endpoint, nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(*e.app_id, *e.cert_id)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.URL.RawQuery = exch_body.Encode()
	return req, nil
}

func (e Config) GenerateIdentityRequest(token string) (*http.Request, error) {
	bearer := "Bearer " + token
	req, err := http.NewRequest("GET", e.identity_endpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", bearer)
	return req, nil
}
