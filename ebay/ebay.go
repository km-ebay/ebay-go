package ebay

type OAuthParams struct {
	Code      string `json:"code"`
	ExpiresIn uint   `json:"expires_in"`
}

type OAuthTokens struct {
	AccessToken           string `json:"access_token"`
	ExpiresIn             uint32 `json:"expires_in"`
	RefreshToken          string `json:"refresh_token"`
	RefreshTokenExpiresIn uint64 `json:"refresh_token_expires_in"`
	TokenType             string `json:"token_type"`
}

type EbayPhone struct {
	CountryCode string `json:"countryCode"`
	Number      string `json:"number"`
	PhoneType   string `json:"phoneType"`
}

type EbayAddress struct {
	AddressLine1    string `json:"addressLine1"`
	AddressLine2    string `json:"addressLine2"`
	City            string `json:"city"`
	StateOrProvince string `json:"stateOrProvince"`
	PostalCode      string `json:"postalCode"`
	Country         string `json:"country"`
}

type EbayName struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type BusinessAccount struct {
	Name           string      `json:"name"`
	Email          string      `json:"email"`
	PrimaryPhone   EbayPhone   `json:"primaryPhone"`
	Address        EbayAddress `json:"address"`
	PrimaryContact EbayName    `json:"primaryContact"`
}

// Needs to be a union
type EbayAccount struct {
	BusinessAccount BusinessAccount `json:"businessAccount"`
}

type EbayIdentity struct {
	UserID                    string          `json:"userId"`
	Username                  string          `json:"username"`
	AccountType               string          `json:"accountType"`
	RegistrationMarketplaceID string          `json:"registrationMarketplaceId"`
	BusinessAccount           BusinessAccount `json:"businessAccount"`
}
